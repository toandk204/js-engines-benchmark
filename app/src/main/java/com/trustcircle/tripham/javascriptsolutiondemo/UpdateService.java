package com.trustcircle.tripham.javascriptsolutiondemo;

import android.content.Context;
import android.util.Log;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import java.io.File;
import java.io.IOException;

/**
 * Created by tripham on 7/19/17.
 */

public class UpdateService {

    // TAG
    private static final String TAG = UpdateService.class.getName();
    // instance of FirebaseRemoteConfig
    private FirebaseRemoteConfig mFirebaseRemoteConfig;
    // context to pass to many classes used by this class
    private Context mContext;

    public UpdateService(Context context) {
        mContext = context;
        // Get Remote Config instance.
        // [START get_remote_config_instance]
        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        // [END get_remote_config_instance]
        // Set default Remote Config parameter values. An app uses the in-app default values, and
        // when you need to adjust those defaults, you set an updated value for only the values you
        // want to change in the Firebase console. See Best Practices in the README for more
        // information.
        // [START set_default_values]
        mFirebaseRemoteConfig.setDefaults(R.xml.remote_config_defaults);
        // [END set_default_values]

        // setup developer mode so that this app can be easily debugged
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build();
        mFirebaseRemoteConfig.setConfigSettings(configSettings);

        // fetch new value from
        fetchVersionAndRunUpdate();

    }

    /**
     * This method fetch new config values from server and download new file if appropriate
     * @return
     */
    public UpdateService fetchVersionAndRunUpdate() {

        // set up FirebaseRemoteConfig so that its cache expire immediately
        final FirebaseRemoteConfig mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build();
        mFirebaseRemoteConfig.setConfigSettings(configSettings);

        long cacheExpiration = 0;

        mFirebaseRemoteConfig.fetch(cacheExpiration).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                mFirebaseRemoteConfig.activateFetched();

                // run the update when we have updated value from server
                runUpdate();
            }
        });

        return this;
    }

    /**
     * Download new JS file if not already in storage
     */
    public void runUpdate() {

        // get newest version number
        String version = mFirebaseRemoteConfig.getString(RemoteConfigConstant.CURRENT_VERSION_KEY);

        // if JS file is not in storage, download new file
        if(isUptoDate(version)) {
            // Newest file available, no download is required
            return;
        } else {
            // // File not found in storage, download new file
            try {
                DownloadUtils.download( mContext, "JavaScript/core_logic_" + version + ".js", version);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     *
     * @param newestVersion newest version fetched from server
     * @return true if the app has newest file stored in storage, false otherwise
     */
    public boolean isUptoDate(String newestVersion) {
        String newestFilePath = mContext.getFilesDir() + "/core_logic_" + newestVersion + ".js";

        File file  = new File(newestFilePath);
        if(file.exists()) {
            Log.i("isUptoDate", "File already exists at " + newestFilePath);
            return true;
        }
        return false;
    }

    /**
     * This method return newest version number based on value from FirebaseRemoteConfig
     * @return newest version number as a String
     */
    public String getNewestVersion() {
        String newestVersion = mFirebaseRemoteConfig.getString(RemoteConfigConstant.CURRENT_VERSION_KEY);
        return newestVersion;
    }
}
