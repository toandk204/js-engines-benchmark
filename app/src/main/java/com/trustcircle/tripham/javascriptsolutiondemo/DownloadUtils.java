package com.trustcircle.tripham.javascriptsolutiondemo;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;

/**
 * Created by tripham on 7/20/17.
 * This class is a helper class that facilitates the task of downloading file from Google Cloud Storage
 *
 */


/**
 * New idea, this class first download the file to a temporary app directory
 * the file is then check for integrity and correctness before saved to application data dir
 */
// TODO this class needed to be modified to accommodate Security Handler class
public class DownloadUtils {

    private static final String TAG = DownloadUtils.class.getName();

    /**
     * This method downloads a file from Google Firebase Storage and then stored it into application storage
     * @param context application context
     * @param pathFromRoot path of file in Google Cloud Storage
     * @param version version to be downloaded
     * @throws IOException
     */
    public static void download(final Context context, final String pathFromRoot, String version) throws IOException{
        // Create a storage reference from our app, see Firebase doc for more info
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReference();

        StorageReference javaScriptRef = storageRef.child(pathFromRoot);

        final File file = new File(context.getFilesDir() + "/core_logic_" + version + ".js");

         javaScriptRef.getFile(file).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
             @Override
             public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {

             }
         }).addOnFailureListener(new OnFailureListener() {
             @Override
             public void onFailure(@NonNull Exception e) {
                Log.e(TAG, "Failed to download " + pathFromRoot);
             }
         });
    }
}
