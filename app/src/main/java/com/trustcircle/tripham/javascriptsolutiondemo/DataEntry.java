package com.trustcircle.tripham.javascriptsolutiondemo;

/**
 * Created by tripham on 7/19/17.
 */

public class DataEntry {
    // input string
    private String mInput;

    // result string
    private String mResult;

    public DataEntry(String input, String result) {
        mInput = input;
        mResult = result;
    }

    public String getInput() {
        return mInput;
    }

    public String getResult() {
        return mResult;
    }
}
