package com.trustcircle.tripham.javascriptsolutiondemo;

import android.content.Context;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by tripham on 7/20/17.
 */
/*
* This class is meant to facilitate the process of storing data
*
* */

public class StorageUtils {

    /**
     * This method was replaced by built in Firebase API
     * This method write a file to Internal Storage given its name, and its byte representation
     * @param context Application contex
     * @param fileName name of the file to be written
     * @param byteData byte[] of the file
     * @return true if the operation was successful, false otherwise
     */
    public static boolean writeFileToInternal(Context context, String fileName, byte[] byteData) {
        FileOutputStream outputStream;

        try {
            outputStream = context.openFileOutput(fileName, Context.MODE_PRIVATE);
            outputStream.write(byteData);
            outputStream.close();
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    /**
     * This method return the String representation of a file from internal storage given its name and application context
     * If no such file is found, it return the default version of the file placed in asset folder
     * @param context application context
     * @param fileName name of the file to be loaded
     * @return String representation of the file got from internal storage
     * @throws IOException
     */
    public static String getBaseJSFromInternal (Context context, String fileName) throws IOException{

        FileInputStream fis = null;
        String baseJS = "";
        try {
            fis = context.openFileInput(fileName);
            baseJS = IoUtils.readInputStream(fis);
        } catch (IOException e) {
            return IoUtils.getJsFromPath(context, "core_logic_default.js");
        } finally {
            fis.close();
        }

        if (baseJS == "") {
            // return the default implementation
            return IoUtils.getJsFromPath(context, "core_logic_default.js");
        }
        return baseJS;

    }

    public static byte[] getByteArrayFromFile(Context context, String filePath) throws IOException {
        FileInputStream fis = context.openFileInput(filePath);

        return IoUtils.getBytesFromInputStream(fis);

    }

}
