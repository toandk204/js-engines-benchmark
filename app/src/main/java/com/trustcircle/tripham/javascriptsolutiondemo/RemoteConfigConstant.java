package com.trustcircle.tripham.javascriptsolutiondemo;

/**
 * Created by tripham on 7/20/17.
 * This class hold remote config constant
 */

public class RemoteConfigConstant {
    public static final String CURRENT_VERSION_KEY = "current_version";
}
