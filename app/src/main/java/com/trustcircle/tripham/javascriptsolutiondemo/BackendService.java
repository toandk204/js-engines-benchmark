package com.trustcircle.tripham.javascriptsolutiondemo;

import android.content.Context;
import com.eclipsesource.v8.V8;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tripham on 7/19/17.
 * This class is used as a data model that provides data for UI components
 */

public class BackendService {

    // Embedded JavaScript engine to execute JavaScript Code
    private V8 mJsEngine;

    // Main Activity context to access storage, etc
    private Context mContext;

    // Update Service required to check and update javascript code
    private UpdateService mUpdateService;
    // to be added file picker, javascript engine, etc ...


    public BackendService(Context context) {
        // initialize js Engine
        //mJsEngine = init(context);
        mContext = context;

        // check for update
        mUpdateService = runUpdateChecker();

        mJsEngine = loadV8WithLogic();
    }

    /**
     * This method initializes JavaScript engine.
     * @param context the context which use this JS engine
     * @param baseJS the JavaScript file to be loaded with JS engine
     * @return JavaScript engine loaded with content from baseJS
     */
    private V8 init(Context context, String baseJS) {
        V8 v8 =  V8.createV8Runtime();
        v8.executeScript(baseJS + ";");
        return v8;
    }

    /**
     * This method is called by RecyclerView adapter to get initial data
     * @return list of DataEntry computed using Formula provided by JavaScript file
     */
    public List<DataEntry> getData() {
        ArrayList<DataEntry> data = new ArrayList<>();

        for(int i = 0; i < 100; i++) {
            int initial = mJsEngine.executeIntegerScript("getNumber()");
            data.add(new DataEntry(String.valueOf(initial), String.valueOf(initial)));
        }
        return data;
    }

    /**
     * check new JavaScript file + update Remote Config values
     * @return UpdateService to be used later
     */
    public UpdateService runUpdateChecker() {
        // run update service for new logic file
        return new UpdateService(mContext).fetchVersionAndRunUpdate();
    }

    /**
     * Initialize V8 engine loaded with appropriate JS file based on the value from server
     * @return V8 engine with loaded JS file
     */
    public V8 loadV8WithLogic() {
        // get version number to load
        String versionToLoad = mUpdateService.getNewestVersion();

        try {
            // get base JavaScript to load into V8
            String baseJS = StorageUtils.getBaseJSFromInternal(mContext, "core_logic_" + versionToLoad + ".js");

            // initialize V8 engine with baseJS
            return init(mContext, baseJS);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null; // failed to load V8
    }

    public void releaseResources() {
        mJsEngine.release();
    }


}
