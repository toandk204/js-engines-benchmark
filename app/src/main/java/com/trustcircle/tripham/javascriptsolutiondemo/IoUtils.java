package com.trustcircle.tripham.javascriptsolutiondemo;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


/**
 * This class facilitates IO operations
 */
public class IoUtils {

    /**
     * This method get the content of a file stored in asset folder and return it as a string
     * @param context application context
     * @param path path of the file
     * @return String representation of the file
     */
    public static String getJsFromPath(Context context, String path) {
        String js = null;
        try {
            InputStream open = context.getAssets().open(path);
            js = readInputStream(open);
        } catch (IOException e) {
            Log.e("IoUtils", e.getLocalizedMessage());
        }
        return js;
    }

    /**
     * this method converts an inputstream in to a String
     * @param ins input stream to be converted
     * @return String content stored in input stream
     * @throws IOException
     */
    public static String readInputStream(InputStream ins) throws IOException {
        BufferedReader streamReader = new BufferedReader(new InputStreamReader(ins));

        StringBuilder stringBuilder = new StringBuilder();
        String inputString;
        while ((inputString = streamReader.readLine()) != null) {
            stringBuilder.append(inputString);
        }

        return stringBuilder.toString();
    }

    public static byte[] getBytesFromInputStream(InputStream is) throws IOException
    {
        ByteArrayOutputStream os = null;
        try {
            os = new ByteArrayOutputStream();
            byte[] buffer = new byte[0xFFFF];

            for (int len; (len = is.read(buffer)) != -1;)
                os.write(buffer, 0, len);

            os.flush();


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            os.close();
        }

        return os.toByteArray();
    }
}
