package com.trustcircle.tripham.javascriptsolutiondemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigInfo;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import java.io.IOException;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getName();
    private TextView mFormulaTextView;
    private BackendService mBackendService;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // set up recycler view
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this); // uses linear layout manager
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL); // set vertical orientation
        recyclerView.setLayoutManager(layoutManager);

        // Inititalize BackendService
        mBackendService = new BackendService(this);

        // Set up adapter
        ModelDataAdapter adapter = new ModelDataAdapter(mBackendService.getData());
        recyclerView.setAdapter(adapter);

        // set formula text view
        mFormulaTextView = (TextView) findViewById(R.id.tv_formula);
        //mFormulaTextView.setText(mBackendService.getFormula());



    }

    private class ModelDataViewHolder extends RecyclerView.ViewHolder {

        public TextView inputText;
        public TextView resultText;


        public ModelDataViewHolder(View itemView) {
            super(itemView);
            inputText = itemView.findViewById(R.id.tv_input);
            resultText = itemView.findViewById(R.id.tv_result);
        }
    }

    private class ModelDataAdapter extends RecyclerView.Adapter<ModelDataViewHolder> {

        private List<DataEntry> dataList;

        public ModelDataAdapter(List<DataEntry> data) {
            this.dataList = data;
        }
        @Override
        public ModelDataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item, parent, false);
            return new ModelDataViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(ModelDataViewHolder holder, int position) {
            DataEntry data = dataList.get(position);

            holder.inputText.setText(data.getInput());
            holder.resultText.setText(data.getResult());

        }

        @Override
        public int getItemCount() {
            return dataList.size();
        }
    }


    @Override
    protected void onStop() {
        super.onStop();

        mBackendService.releaseResources();

    }
}
