package com.trustcircle.tripham.javascriptsolutiondemo;

import android.content.Context;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 * This class help verify the integrity of file downloaded from the internet
 * Created by tripham on 7/21/17.
 */

public class SecurityHandler {
    // ideally, this key need to be stored securely, not hardcoded into the application
    // eg receive from the internet (not ideal), or other methods that can hide it from reverse engineering
    // optionally, proguard can be use to harden reverse engineer
    private static final String PRIVATE_KEY = "";
    // TODO implement security

    private Context mContext;


    public SecurityHandler(Context context) {
        mContext = context;
    }

    /**
     * This method check the file integrity by generating its hash value and the compare with the given hash value
     * Note: The hash value is intended to be sent by the application server
     * @param inputFile The file to be checked with hashValue
     * @param hashValue The hash value to check against the inputFile's hash value (hexadecimal)
     * @return return true if the file has identical has value with hashValue, false otherwise
     */
    public static boolean verifyFileWithHash(File inputFile, String hashValue) throws IOException{

        MessageDigest digest = getMessageDigest("SHA-256");

        //Get file input stream for reading the file content
        FileInputStream fis = new FileInputStream(inputFile);

        //Create byte array to read data in chunks
        byte[] byteArray = new byte[1024];
        int bytesCount = 0;

        //Read file data and update in message digest
        while ((bytesCount = fis.read(byteArray)) != -1) {
            digest.update(byteArray, 0, bytesCount);
        }

        //close the stream; We don't need it now.
        fis.close();

        //Get the hash's bytes
        byte[] bytes = digest.digest();

        //This bytes[] has bytes in decimal format;
        //Convert it to hexadecimal format
        StringBuilder sb = new StringBuilder();
        for(int i=0; i< bytes.length ;i++)
        {
            sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
        }

        return sb.toString().toLowerCase().equals(hashValue.toLowerCase());
    }


    public byte[] decryptFile(String inFile, PrivateKey privateKey) {
        byte[] encryptedByte = null;
        byte[] decryptedByte = null;
        try {
            encryptedByte = StorageUtils.getByteArrayFromFile(mContext, inFile);
            decryptedByte = decryptFileHelper(encryptedByte, privateKey);
        } catch (Exception e){
            e.printStackTrace();
        }
        return decryptedByte;
    }

    private byte[] decryptFileHelper(byte[] encryptedBytes, PrivateKey privateKey) throws InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException, BadPaddingException, IllegalBlockSizeException {
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        return cipher.doFinal(encryptedBytes);
    }


    /**
     * This method returns a MessageDigest object which is initialized with a given algorithm
     * @param algorithm the algorithm to initialize the message digest with
     * @return a MessageDigest initialized with the given algorithm     */
    private static MessageDigest getMessageDigest(String algorithm) {
        MessageDigest digest = null;

        try {
            digest = MessageDigest.getInstance(algorithm);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return digest;
    }


    public static PrivateKey getPrivateKey(Context context, String fileName) throws Exception {
        InputStream fis = context.getAssets().open(fileName);
        byte[] keyBytes = IoUtils.getBytesFromInputStream(fis);

        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePrivate(spec);
    }

}
